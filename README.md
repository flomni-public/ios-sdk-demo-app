# `Интеграция FlomniChat SDK в ваше iOS приложение`

Этот гайд поможет вам интегрировать FlomniChat SDK в ваше iOS приложение.

# Шаг 1. Установка пакета

[![Latest Release](https://gitlab.com/flomni-public/ios-sdk-demo-app/-/badges/release.svg)](https://gitlab.com/flomni-public/ios-sdk-demo-app/-/releases)
- ## CocoaPods

Добавьте FlomniChatCore в ваш Podfile:

```ruby
platform :ios, '15.0'

target 'YOUR_TARGET_NAME' do
    use_frameworks!
    # ... здесь могут быть другие зависимости
    pod 'FlomniChatSDK'
end
```

Затем через `Terminal` выполните команду:

```bash
pod install --repo-update 
```

- ## Swift Package Manager

В Xcode проекте через `File > Swift Packages > Add Package Dependency...` и укажите URL репозитория FlomniChat:

```
git@gitlab.com:flomni-public/ios-sdk-demo-app.git
```

![Демо SwiftPackageManager](.assets/DemoSPM.jpg)

# Шаг 2. Настройка проекта

> **!!!** Убедитесь, что `FlomniChatCore.framework` добавлен во все таргеты вашего проекта. Это можно проверить в настройках таргетов в Xcode.
> ![Добавление SDK во все таргеты](.assets/XcodeProject.jpg)

### Настройка `Info.plist`

- Добавьте домен `link.dev.flomni.com` и `i.dev.flomni.com` в **Target Properties** для корректной работы SDK с **API**:

```xml
<key>NSExceptionDomains</key>
<dict>
<key>i.dev.flomni.com</key>
<dict>
<key>NSExceptionAllowsInsecureHTTPLoads</key>
<true/>
<key>NSIncludesSubdomains</key>
<true/>
</dict>
<key>link.dev.flomni.com</key>
<dict>
<key>NSExceptionAllowsInsecureHTTPLoads</key>
<true/>
<key>NSIncludesSubdomains</key>
<true/>
</dict>
</dict>
```

- Для работы функционала отправки медиа в чате следует добавить описание использования **Камеры** и **Библиотеки Фото**:
```xml
<key>NSCameraUsageDescription</key>
<string> Flomni Chat needs permission to Camera for user aviability to capture media to sent in chat.</string>

<key>NSPhotoLibraryUsageDescription</key>
<string> Flomni Chat needs permission to Photo Library for user aviability to pick media to sent in chat.</string>
```

- Для корректной работы уведомлений - **Push Notifications**, добавьте:
```xml
<key>UIBackgroundModes</key>
<array>
<string>fetch</string>
<string>remote-notification</string>
</array>
```
> #### Конечный вид Info.plist
> ![Конечный вид Info.plist](.assets/InfoPlist.jpg)

## Шаг 3: Инициализация SDK

### Импорт необходимых модулей

Убедитесь, что вы импортировали все необходимые модули в вашем `AppDelegate`:

```swift
import FlomniChat
import SwiftUI
import UIKit
import UserNotifications
```

### Настройка `AppDelegate`

Создайте и настройте класс `AppDelegate`, который будет инициализировать FlomniChat и обрабатывать уведомления:

Создаем и настраиваем ``ChatClient``

#### Параметры:
- `companyId`: Ключ компании. Этот ключ можно получить, зарегистрировавшись на [сайте Flomni](https://my.stg.flomni.com/).
- `appGroup`: Идентификатор группы приложений для локального хранилища.
- `userId`: Уникальный идентификатор пользователя (По умолчанию равен [`DeviceID`](doc://com.apple.documentation/documentation/uikit/uidevice/1620059-identifierforvendor)) .
- `contour`: enum состоящий из трех кейсов: production, develop, custom(baseURL: String).
- `visitorAttributes`: кастомные атрибуты, относящиеся к пользователю приложения при подключении к чату.
- `theme`: цветовая тема модуля Flomni.
- `locale`: принудительная установка локализации: .english, .russian.
- `chatCreationMode`: енам, определяющий режим создания чатов при нажатии кнопки "Отправить сообщение" в списке чатов. Содержит два случая: 
  - .forceCreateChats - всегда создает новый чат, независимо от существующих, 
  - .existingChats - если чат по теме уже существует, открывает его; если не существует — создает новый.

```swift
@main
class AppDelegate: UIResponder, UIApplicationDelegate {
        var window: UIWindow ?
        /// Экземпляр `ChatModule`, предоставляемый `AppDelegate`
        var flomniChat: ChatModule?

        func application(
            _ application: UIApplication,
            didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey:Any] ?
        ) -> Bool {
            /// Create and configure the `ChatModule` instance
            flomniChat = ChatModule(
                        companyID: "5d0cd1707741de0009e061cb",
                        appGroup: "group.com.flomni.chat",
                        contour: .production, // .custom("link.example.com"),
                        userId: UIDevice.deviceId,
                        visitorAttributes: "{\"test\":\"testData\",\"environment\":\"iOS_Client\"}",
                        theme: createChatTheme(),
                        locale: .russian,
                        chatCreationMode: .forceCreateChats
                    )
            return true
	}
```
#### Кастомизация темы:
- `colorIconPalette`: палитра цветов в виде массива HEX-кодов. Цвет выбирается на основе определённого свойства хеша чата
- `generalColors`: основные цвета
- `messageColors`: цвета сообщений в чате
- `inputFieldColors`: цвета поля ввода сообщения  

```swift
/// Пример кастомизации темы
    private static func createChatTheme() -> ChatTheme {
        ChatTheme.init(
            colorIconPalette: ["ccff00", "e7e9e8", "176339"],
            generalColors: .init(
                accent: (light: "#17b339", dark: "#17b339"), // Главный цвет
                background: (light: "#ffffff", dark: "#ffffff"), // Общий фон
                primaryText: (light: "#000000", dark: "#000000"), // Основной текст
                secondaryText: (light: "#a3a8a6", dark: "#a3a8a6") // Второстепенный текст
            ),
            messageColors: .init( //Собщения в чате
                inboundText: (light: "#000000", dark: "#000000"), // Текcт входящего сообщения
                outboundText: (light: "#ffffff", dark: "#ffffff"), // Текcт исходящего сообщения
                inboundBackground: (light: "#17b339", dark: "#17b339"), // Фон исходящего сообщения
                outboundBackground: (light: "#e7e9e8", dark: "#e7e9e8"), // Фон входящего сообщения
                borderStroke: (light: "#B5CADD", dark: "#262529"), // Обводка границы
                captionText: (light: "#17b339", dark: "#17b339"), // Подписи под сообщением
                buttonColor: (light: "#808080", dark: "#808080") // Фон кнопок
            ),
            inputFieldColors: .init( // Поле ввода сообщения
                sendButtonBackground: (light: "#17b339", dark: "#17b339"), // Фон
                sendButtonTint: (light: "#f6fa7f", dark: "#4676DA"), // Текст
                inputFieldBackground: (light: "#FFFFFF", dark: "#1E1E1E"), // Поле ввода фон
                fieldStroke: (light: "#787880", dark: "#1E1E1E"), // Поле ввода Обводка
                attachButton: (light: "#8E8E93", dark: "#E6E6E6"), // Кнопка `+`
                barBackground: (light: "#556832", dark: "#4676DA") // Фон бара
            )
        )
    }
}
```
#### FlomniChatDelegate methods:
- `handle(url: URL)`: метод для обработки нажатий на ссылки в чате. Если делегат не установлен, применяется поведение по умолчанию при нажатии на ссылки.

```swift
@main
class AppDelegate: UIResponder, UIApplicationDelegate {
        var window: UIWindow ?
        var flomniChat: ChatModule?

        func application(
            _ application : UIApplication,
            didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey:Any] ?
        )->Bool {
            flomniChat = ChatModule(
                        companyID: "5d0cd1707741de0009e061cb",
                        appGroup: "group.com.flomni.chat",
                        contour: .production, // .custom("link.example.com"),
                        userId: UIDevice.deviceId,
                        visitorAttributes: "{\"test\":\"testData\",\"environment\":\"iOS_Client\"}"
                    )
            ChatModule.delegate = self
            return true
	}
}
extension AppDelegate: FlomniChatDelegate {
    func handle(url: URL) {
        print(url)
    }
}
```

## Шаг 3: Показ списка чатов

### Настройка отображения модуля на `UIKit`

Создайте новый `UIHostingController`, который будет отображать интерфейс FlomniChat:

```swift
import FlomniChat
import SwiftUI
import UIKit

/// Инициализируем основной интерфейс `FlomniChat` списка каналов
/// - Параметры:
/// - settings: структура NavigationSettings, имеет опциональные значения leadingView, centerView и trailingView, для отображения элементов в Navigation Bar.
/// - widgetSettings: структура WidgetSettings, опционально можно настроить нижний виджет на экране списка чатов

class ChatViewController: UIHostingController<AnyView> {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder, rootView: AnyView(ChatModule.Content(
            widgetSettings: WidgetSettings(startMessage: "Напишите нам", descriptionText: "Мы тут 7 дней в неделю!"),
            settings: NavigationSettings(
                        leadingView: {
                            Button (action: { print("Test back") }) {
                                Image(systemName: "chevron.left")
                                    .tint(.gray)
                                    .frame(width: 32, height: 32)
                            }
                        }, centerView: {
                            Text("Title")
                                .font(.system(size: 17, weight: .semibold))
                        }, trailingView: {
                            Button (action: { print("Test info") }) {
                                Image(systemName: "info.circle")
                                    .tint(.gray)
                            }
                        })))
        )
    }
}
```
### Настройка отображения модуля на `SwiftUI`
- Укажите `UIApplicationDelegateAdaptor` в структуре типа `App` вашего приложения 
```swift
@main
struct DemoApp: App {
    @UIApplicationDelegateAdaptor(AppDelegate.self) var delegate
    var body: some Scene {
        WindowGroup {
            ChatModule.Content(
                widgetSettings: WidgetSettings(startMessage: "Напишите нам", descriptionText: "Мы тут 7 дней в неделю!"),
                settings: 
                    NavigationSettings(
                        leadingView: {
                            Button (action: { print("Test back") }) {
                                Image(systemName: "chevron.left")
                                    .tint(.gray)
                                    .frame(width: 32, height: 32)
                            }
                        }, centerView: {
                            Text("Title")
                                .font(.system(size: 17, weight: .semibold))
                        }, trailingView: {
                            Button (action: { print("Test info") }) {
                                Image(systemName: "info.circle")
                                    .tint(.gray)
                            }
                        }))
        }
    }
}
```
## Шаг 4: Показ чата
### Настройка отображения модуля на `UIKit`

Создайте новый `UIHostingController`, который будет отображать интерфейс FlomniChat:

```swift
import FlomniChat
import SwiftUI
import UIKit

/// Инициализируем основной интерфейс `FlomniChat` списка каналов
/// - Параметры:
///   - id: внешний externalChatId (например: uuid заказа/товара)
///   - name: наименование чата
///   - icon: иконка чата типа SFSymbol, например SFSymbol.cartFill.name
///   - metaData: кастомные атрибуты типа [String: Any], относящиеся к пользователю приложения при подключении к чату.
class ChatViewController: UIHostingController<AnyView> {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder, rootView:
            AnyView(ChatModule.openOrCreateNewChat(
                id: "007_example", 
                name: "Вопрос по заказу", 
                icon: SFSymbol.cartFill.name,
                metaData: ["test": "example"]
            ))
        )
    }
}
```

### Шаг 4: Тестирование

**Запустите приложение** и убедитесь, что оно успешно инициализирует `FlomniChat`.

Теперь ваше приложение интегрировано с Flomni Chat SDK и готово для обработки сообщений и уведомлений.
