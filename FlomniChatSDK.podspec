#
# Be sure to run `pod lib lint FlomniChatSDK.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |spec|

spec.name             = 'FlomniChatSDK'
spec.version          = '1.1.1'
spec.summary          = 'A SwiftUI-based chat module for embedding in client applications.'
spec.description      = "FlomniChat is a chat module built with SwiftUI that can be embedded into client applications."
spec.homepage     = "https://gitlab.com/flomni-public/ios-sdk-demo-app"
spec.author           = { 'Stefan' => 'stefanboblic@gmail.com', 'Flomni, Inc.' => 'welcome@flomni.ru' }
spec.license = { :type => 'MIT', :text => <<-LICENSE
	© Copyright 2024 ООО "Омнитек". Все права защищены. Все товарные знаки являются собственностью их соответствующих владельцев.
	Flomni Omnichannel Platform зарегистрирована в РРПО. Реестровая запись №17699 от 19.05.2023.
	LICENSE
}
spec.social_media_url = 'https://flomni.com/ru/products/in-app-chat/'

spec.source       = { :http => "https://gitlab.com/api/v4/projects/57840286/packages/generic/FlomniChatCore/1.1.0/FlomniChatCore.zip", :flatten => false }
spec.preserve_paths = '*'
spec.ios.deployment_target = '15.0'
spec.swift_version = '5.9'

spec.pod_target_xcconfig = {
	'IPHONEOS_DEPLOYMENT_TARGET' => '15.0',
	'ENABLE_USER_SCRIPT_SANDBOXING' => 'NO'
}
spec.vendored_frameworks = 'FlomniChat.xcframework'
end
