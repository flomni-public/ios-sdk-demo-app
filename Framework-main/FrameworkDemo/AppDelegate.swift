//
//  Flomni_ChatApp.swift
//  Flomni Chat
//
//  Created by Dmitry Mikhaylov on 07.06.2024.
//

import FlomniChat
import SwiftUI
import Sentry

// MARK: - Flomni_ChatApp

@main
struct Flomni_ChatApp: App {

    @UIApplicationDelegateAdaptor(AppDelegate.self) var appDelegate

	init() {
        SentrySDK.start { options in
            options.dsn = "https://540d6e0bbbfce3761500aab001eef7be@o4507423950700544.ingest.de.sentry.io/4507680470925392"
            options.debug = true // Enabled debug when first installing is always helpful
            
            options.onCrashedLastRun = { error in
                SentrySDK.capture(event: error)
            }
            // Uncomment the following lines to add more data to your events
            options.attachScreenshot = true // This adds a screenshot to the error events
            options.attachViewHierarchy = true // This adds the view hierarchy to the error events
            options.attachViewHierarchy = true
            options.enableMetricKit = true
            options.enableTimeToFullDisplayTracing = true
            options.swiftAsyncStacktraces = true
            options.enableAppLaunchProfiling = true
            options.enableMetrics = true
            options.experimental.sessionReplay = .init(sessionSampleRate: 1.0, onErrorSampleRate: 1.0, redactAllText: false, redactAllImages: false)
            options.enableUserInteractionTracing = true
            // Example uniform sample rate: capture 100% of transactions
            // In Production you will probably want a smaller number such as 0.5 for 50%
            options.tracesSampleRate = 1.0
        }
	}
	
	var body: some Scene {
		WindowGroup {
			ChatModule.DebugTabView()
		}/*.environment(\.managedObjectContext, (appDelegate.flomniChat?.persistence.modelExecutor.context)!)*/
	}
}

// MARK: - AppDelegate

class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {
	var window: UIWindow?
	/// Экземпляр `FlomniChat`, предоставляемый `AppDelegate`
	var flomniChat: ChatModule?
	
	func application(
		_: UIApplication,
		didFinishLaunchingWithOptions _: [UIApplication.LaunchOptionsKey: Any]?
	) -> Bool {
		// Create and configure the `FlomniChat` instance
		flomniChat = ChatModule(
			companyID: "5d0cd1707741de0009e061cb",
			appGroup: "group.com.flomni.chat",
            contour: .develop,
			userId: UIDevice.deviceId,
			visitorAttributes: "{\"test\":\"testData\",\"environment\":\"iOS_Client\"}"
		)
		
		return true
	}
	
	/// Сообщает делегату, что приложение успешно зарегистрировано в Apple Push Notification service (APNs).
	func application(
		_: UIApplication,
		didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data
	) {
		// Передаём токен устройства во `FlomniChat`
		flomniChat?.didRegisterForRemoteNotifications(with: deviceToken)
	}
	
	/// Сообщает делегату, что приложение получило удаленное уведомление.
	func application(
		_: UIApplication,
		didReceiveRemoteNotification userInfo: [AnyHashable: Any],
		fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void
	) {
		flomniChat?.didReceiveRemoteNotification(
			didReceiveRemoteNotification: userInfo,
			fetchCompletionHandler: completionHandler
		)
	}
	
	func userNotificationCenter(
		_: UNUserNotificationCenter,
		willPresent _: UNNotification,
		withCompletionHandler _: @escaping (UNNotificationPresentationOptions) -> Void
	) {}
	
	/// Обработка ошибок
	func application(_: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
		debugPrint(error)
	}
}
