//
//  ChatViewController.swift
//  FlomniChat Demo

import FlomniChat
import SwiftUI
import UIKit

/// Initialize the `FlomniChat` main interface of Channels List
/// - Parameters:
///   - theme : The preconfigured `FlomniChat` theme of chat module.
///   - route: The `FlomniChat` route for navigation to ChannelID, recieved from message's body of APNs Push Notification.
///   - placement: The placement of custom toolbar pushed to the top of chats list view in the ChatCorner.
/// - content: The custom content to be displayed in the navigation bar of chat module.
class ChatViewController: UIHostingController<AnyView> {
	@Injected(\.flomniChat) var flomniChat
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder, rootView: AnyView(ChatModule.DebugTabView()))
	}
}
