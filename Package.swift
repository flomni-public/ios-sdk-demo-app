// swift-tools-version: 5.9
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "FlomniChat",
    defaultLocalization: "ru",
    platforms: [.iOS(.v15)],
    products: [
        // Products define the executables and libraries a package produces, making them visible to other packages.
        .library(
            name: "FlomniChat",
            targets: ["FlomniChat"]
        ),
    ],
    dependencies: [],
    targets: [
        // Targets are the basic building blocks of a package, defining a module or a test suite.
        // Targets can depend on other targets in this package and products from dependencies.
        .binaryTarget(
            name: "FlomniChat",
            url: "https://gitlab.com/api/v4/projects/57840286/packages/generic/FlomniChatCore/1.1.1/FlomniChatCore.zip",
            checksum: "60fd802f1848dd4faf8b2cacc025555f8ddee834d311d9c370e9046e235f9ad1"
        )
    ],
    swiftLanguageVersions: [.v5]
)
